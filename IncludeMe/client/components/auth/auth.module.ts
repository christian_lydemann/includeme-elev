'use strict';

angular.module('includeMeApp.auth', [
  'includeMeApp.constants',
  'includeMeApp.util',
  'ngCookies',
  'ui.router'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
