'use strict';

class NavbarController {
  //start-non-standard
  menu = [{
      'title': 'Hjem',
      'state': 'main'
    },
    {
      'title': 'Oversigt',
      'state': 'studentOverview'
    },
    {
      'title': 'Dagsorden',
      'state': 'followAgenda'
  }];

  isCollapsed = true;
  //end-non-standard

  constructor(Auth) {
    this.isLoggedIn = Auth.isLoggedIn;
    this.isAdmin = Auth.isAdmin;
    this.getCurrentUser = Auth.getCurrentUser;
  }
}

angular.module('includeMeApp')
  .controller('NavbarController', NavbarController);
