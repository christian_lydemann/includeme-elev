'use strict';

angular.module('includeMeApp')
    .directive('assignmentExercise', () => ({
        templateUrl: 'components/student/directives/assignmentExercise.html',
        restrict: 'E',
        scope: {
            exercise: '='
        }
    }));
