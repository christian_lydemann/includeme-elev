'use strict';

angular.module('includeMeApp')
    .controller('linkExerciseCtrl', function ($sce) {
        this.trustSrc = function (src: String) {
            return $sce.trustAsResourceUrl(src);
        };
    })
    .directive('linkExercise', () => ({
        restrict: 'E',
        scope: {
            exercise: '='
        },
        controller: 'linkExerciseCtrl',
        controllerAs: 'linkExercise',
        templateUrl: 'components/student/directives/linkExercise.html'
    }));
