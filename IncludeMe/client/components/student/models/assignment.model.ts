/**
 * Created by chris on 23-02-2016.
 */

namespace Models {
    export class Assignment {

        constructor() {
            this.isMultipleChoice = false;
        }

        _id: number;
        description: string;
        isMultipleChoice: boolean;
        options: string[]; // if isMultipleChoice
        studentAnswer: string;
        correctAnswer: string;
    }
}
