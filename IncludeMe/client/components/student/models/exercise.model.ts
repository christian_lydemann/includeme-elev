
namespace Models {
    export class Exercise {

        constructor() {
            this.isLink = false;
            this.validatorStyle = "exercise-success";
        }

        _id: number;
        name: string;
        description: string;
        question: string;
        points: number;
        assignments: Models.Assignment[];
        isLink: boolean;
        link: string; // if isLink = true
        validatorStyle: string;
        activeStyle: string;
    }
}
