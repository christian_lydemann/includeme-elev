/**
 * Created by chris on 23-02-2016.
 */
namespace Student {

    export class StudentService {

        exercises: Models.Exercise[];

        constructor () {

            // fake data
            // Assignments
            var assignment1 = new Models.Assignment();
            assignment1._id = 0;
            assignment1.description = "25/5 =";
            assignment1.correctAnswer = "5";

            var assignment2 = new Models.Assignment();
            assignment2._id = 1;
            assignment2.description = "20/4 =";

            assignment2.correctAnswer = "5";

            var assignment3 = new Models.Assignment();
            assignment3._id = 2;
            assignment3.description = "14/2 =";
            assignment3.correctAnswer = "7";

            var assignment4 = new Models.Assignment();
            assignment4._id = 3;
            assignment4.description = "14 + 10 / 2 =";
            assignment4.correctAnswer = "19";

            // MultipleChoice
            var multipleChoice1 = new Models.Assignment();
            multipleChoice1._id = 0;
            multipleChoice1.description = "40/5 =";
            multipleChoice1.isMultipleChoice = true;
            multipleChoice1.options = ['4', '6', '9', '8'];
            multipleChoice1.studentAnswer;
            multipleChoice1.correctAnswer = "8";


            var multipleChoice2 = new Models.Assignment();
            multipleChoice2._id = 1;
            multipleChoice2.description = "36/4 =";
            multipleChoice2.isMultipleChoice = true;
            multipleChoice2.options = ['8', '9', '7', '10'];
            multipleChoice2.studentAnswer;
            multipleChoice2.correctAnswer = "9";

            // Exercises
            var exercise1 = new Models.Exercise(); // type 0: basic assignments
            exercise1._id = 0;
            exercise1.name = "Opgave 1: Lille nem opgave";
            exercise1.description = "Løs følgende opgaver";
            exercise1.assignments = [assignment1, assignment2, assignment3, assignment4];
            exercise1.points = 2;

            var exercise2 = new Models.Exercise(); // type 2: link
            exercise2._id = 1;
            exercise2.name = "Opgave 2: Dividerings opgaver fra Matematikfessor";
            exercise2.description = "Læs følgende side på Matematikfessor";
            exercise2.link = "https://www.matematikfessor.dk/lessons/division-433";
            exercise2.points = 5;
            exercise2.isLink = true;

            var exercise3 = new Models.Exercise(); // type 1: multiple choice
            exercise3._id = 2;
            exercise3.name = "Opgave 3: Multiple choice opgave";
            exercise3.description = "Løs følgende opgaver";
            exercise3.assignments = [multipleChoice1, multipleChoice2];
            exercise3.points = 3;

            this.exercises = [exercise1, exercise2, exercise3];
        }

        getExercises() {
            return this.exercises;
        }
    }

    angular.module('includeMeApp').factory("StudentService",
        function()
        {
            return new StudentService();
        }
    );
}
