'use strict';

angular.module('includeMeApp.admin', [
  'includeMeApp.auth',
  'ui.router'
]);
