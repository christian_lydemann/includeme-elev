'use strict';

angular.module('includeMeApp', [
      'includeMeApp.auth',
      'includeMeApp.admin',
      'includeMeApp.constants',
      'ngCookies',
      'ngResource',
      'ngSanitize',
      'btford.socket-io',
      'ui.router',
      'ui.bootstrap',
      'validation.match'
    ])
    .config(function($urlRouterProvider, $locationProvider) {
      $urlRouterProvider
          .otherwise('/');

      $locationProvider.html5Mode(true);
    })
    .config(['$sceDelegateProvider', function($sceDelegateProvider)
    {
        $sceDelegateProvider.resourceUrlWhitelist(['self', /^https?:\/\/(mhnystatic\.)?s3.amazonaws.com/,
            /^https?:\/\/(static\.)?s3.amazonaws.com/]);
    }]);
