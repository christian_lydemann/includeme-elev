'use strict';

angular.module('includeMeApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('studentOverview', {
        url: '/student-overview',
        templateUrl: 'app/student/studentOverview/studentOverview.html',
        controller: 'StudentOverviewCtrl'
      });
  });
