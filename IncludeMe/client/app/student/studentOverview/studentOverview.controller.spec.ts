'use strict';

describe('Controller: StudentOverviewCtrl', function () {

  // load the controller's module
  beforeEach(module('includeMeApp'));

  var StudentOverviewCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentOverviewCtrl = $controller('StudentOverviewCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
