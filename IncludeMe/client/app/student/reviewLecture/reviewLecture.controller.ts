'use strict';
namespace ReviewLecture {

    class ReviewLectureCtrl {

        exercises: Models.Exercise[] = [];
        points: number = 0;
        reviewScore: number = 5;
        constructor (private StudentService) {
            var exercises = StudentService.getExercises();

            this.validateAnwers(exercises);

            for (var i in exercises) {
                if (exercises[i] !== undefined) {
                    this.exercises.push(exercises[i]);
                }
            }
        }

        // validate exercises
        validateAnwers(exercises) {
            var success = "exercise-success";
            var errorCss = "exercise-error";
            for (var i = 0; i < exercises.length; i++) {
                var errorInExercise = false;
                if (!exercises[i].isLink) {
                    for (var idx = 0; idx < exercises[i].assignments.length; idx++) {

                        // if one assign is wrong in an exercise, give no points, set errorCss and go to next exercise
                        if (exercises[i].assignments[idx].studentAnswer !== exercises[i].assignments[idx].correctAnswer) { // if wrong
                            exercises[i].validatorStyle = errorCss;
                            exercises[i].assignments[idx].isWrong = true;
                            errorInExercise = true;
                        }
                    }
                    if (errorInExercise === false) {
                        exercises[i].validatorStyle = success;
                        this.points += exercises[i].points;
                    }
                } else {
                    this.points += exercises[i].points;
                }
            }
        }

        getStyle (idx) {
            return this.exercises[idx].validatorStyle;
        }
    }

    angular.module('includeMeApp')
        .controller('ReviewLectureCtrl', ReviewLectureCtrl);
}
