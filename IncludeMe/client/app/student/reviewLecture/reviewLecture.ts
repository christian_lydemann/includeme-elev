'use strict';
namespace ReviewLecture {
angular.module('includeMeApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('reviewLecture', {
                url: '/review-lecture',
                templateUrl: 'app/student/reviewLecture/reviewLecture.html',
                controller: 'ReviewLectureCtrl',
                controllerAs: 'reviewLecture'
            });
    });
}
