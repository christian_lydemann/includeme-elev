'use strict';

angular.module('includeMeApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('visOpgaver', {
                url: '/vis-opgaver',
                templateUrl: 'app/student/visOpgaver/visOpgaver.html',
                controller: 'VisOpgaverCtrl',
                controllerAs: 'visOpgaver'
            });
    });
