'use strict';

describe('Controller: VisOpgaverCtrl', function () {

  // load the controller's module
  beforeEach(module('includeMeApp'));

  var VisOpgaverCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    VisOpgaverCtrl = $controller('VisOpgaverCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
