'use strict';
namespace VisOpgaver {

    class VisOpgaverCtrl {

        exercises: Models.Exercise[] = [];
        constructor (private StudentService) {
            var exercises = StudentService.getExercises();

            for (var i in exercises) {
                if (exercises[i] !== undefined) {
                    this.exercises.push(exercises[i]);
                }
            }
        }
    }

    angular.module('includeMeApp')
        .controller('VisOpgaverCtrl', VisOpgaverCtrl);
}
