'use strict';

angular.module('includeMeApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('followAgenda', {
                url: '/follow-agenda/:idx',
                templateUrl: 'app/student/followAgenda/followAgenda.html',
                controller: 'FollowAgendaCtrl',
                controllerAs: 'followAgenda'
            });
    });
