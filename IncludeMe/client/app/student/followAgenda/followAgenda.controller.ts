'use strict';
namespace followAgenda {
  class FollowAgendaCtrl {
    currentExercise: Models.Exercise;
    exercises: Models.Exercise[] = [];
    currentIdx: number = 0;

    constructor (private StudentService: Student.StudentService, $stateParams) {
      var exercises = StudentService.getExercises();

      for (var i in exercises) {
        if (exercises[i] !== undefined) {
          this.exercises.push(exercises[i]);
        }
      }

      // get stateParam and set currentExercise
      var idx = $stateParams.idx || '0';
      this.currentIdx = parseInt(idx);
      this.currentExercise = this.exercises[this.currentIdx];
    }

    getActiveClass (itemIdx) {
      if (itemIdx === this.currentIdx) {
        // set active css class
        return "active";
      }
    }

  }

  angular.module('includeMeApp')
      .controller('FollowAgendaCtrl', FollowAgendaCtrl);
}
